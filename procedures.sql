create or replace procedure add_publisher(
   id uuid,
   publisherName text
)
language plpgsql
as $$
begin
	insert into public."Publishers";
	values(id, publisherName, 0);
end;
$$;

create or replace procedure add_player(
   id uuid,
   playerName text
)
language plpgsql
as $$
begin
	insert into public."Players"
	values(id, playerName, 0);
end;
$$
