namespace Gx.User
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using Gx.User.Configuration;
    using Gx.User.Entities;
    using Gx.User.Services;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.IdentityModel.Tokens;
    using Microsoft.OpenApi.Models;
    using Org.BouncyCastle.Crypto;
    using Org.BouncyCastle.Crypto.Generators;
    using Org.BouncyCastle.Crypto.Parameters;
    using Org.BouncyCastle.OpenSsl;
    using Org.BouncyCastle.Security;
    using ScottBrady.IdentityModel.Tokens;

    /// <summary>
    /// Class, responsible for configuring the app, tuning of services and request pipes.
    /// </summary>
    public class Startup
    {
        private const string PublicCorsPolicyName = "PublicPolicy";
        private const string LoginProvider = "GX";

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="env">Object, responsible for the application environment (get and interact with it).</param>
        /// <param name="configuration">Gathered configuration object.</param>
        public Startup(IWebHostEnvironment env, IConfiguration configuration)
        {
            CurrentEnvironment = env;
            Configuration = configuration;
        }

        private IWebHostEnvironment CurrentEnvironment { get; }

        private IConfiguration Configuration { get; }

        /// <summary>
        /// Register services, which are necessary for the app.<br/>
        /// Methods have signature "Add[ServiceName]".
        /// </summary>
        /// <param name="services">Collection of services to add.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(PublicCorsPolicyName, builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.Configure<DbConfiguration>(Configuration.GetSection("DbConfig"));

            var (publicKey, privateKey, publicKeyString) = ReadKeys();

            var jwtConfig = Configuration.GetSection("JwtConfiguration").Get<JwtConfiguration>();

            services.Configure<JwtConfiguration>(Configuration.GetSection("JwtConfiguration"));
            services.Configure<JwtConfiguration>(options =>
            {
                options.PublicKey = publicKey;
                options.PrivateKey = privateKey;
                options.PublicKeyString = publicKeyString;
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtConfig.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = publicKey,
                ValidAudience = jwtConfig.Audience,
                ValidateAudience = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.FromMinutes(1),
            };

            services.AddSingleton<IJwtService, JwtService>();
            services.AddSingleton(tokenValidationParameters);

            services.AddAuthentication("Bearer").AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.RequireHttpsMetadata = false;
                jwtBearerOptions.SaveToken = true;
                jwtBearerOptions.TokenValidationParameters = tokenValidationParameters;
            });

            services.AddDbContext<UserDbContext>();

            services.AddIdentity<IdentityUser, IdentityRole>(options =>
                {
                    options.Password.RequiredLength = 8;
                    options.Password.RequireLowercase = true;
                    options.Password.RequireUppercase = true;
                    options.Password.RequireNonAlphanumeric = true;
                }).AddEntityFrameworkStores<UserDbContext>()
                .AddTokenProvider(LoginProvider, typeof(DataProtectorTokenProvider<IdentityUser>));

            services.AddScoped<IAccountService, AccountService>();

            services.AddControllers();

            var appVersion = Configuration["AppVersion"];
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(appVersion, new OpenApiInfo
                {
                    Version = appVersion,
                    Title = "Gx.User service API",
                });

                // Set the comments path for the Swagger JSON and UI.
                // Source: https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.1&tabs=visual-studio#xml-comments
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        /// <summary>
        /// Configures, how the app will proceed requests.
        /// </summary>
        /// <param name="app">Object, responsible for setting components of the app.<br/>
        /// Methods have signature "Use[ComponentName]".</param>
        /// <param name="env">Object, responsible for the application environment (get and interact with it).</param>
        /// <param name="context">Database context.</param>
        /// <param name="roleManager">Role manager.</param>
        // ReSharper disable once UnusedMember.Global
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserDbContext context, RoleManager<IdentityRole> roleManager)
        {
            if (env.IsDevelopment())
            {
                app.UseExceptionHandler("/error/development");
            }
            else
            {
                app.UseExceptionHandler("/error/production");
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseCors(PublicCorsPolicyName);

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "swagger/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"{Configuration["AppVersion"]}/swagger.json", "Gx.User service API");
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            UpdateDatabases(context, roleManager);

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private static AsymmetricKeyParameter ReadAsymmetricKeyParameterFromBase64String(string base64String)
        {
            var decodedPemString = Convert.FromBase64String(base64String);
            var stringReader = new StringReader(Encoding.UTF8.GetString(decodedPemString));
            var pemReader = new PemReader(stringReader);
            var keyParameter = (AsymmetricKeyParameter)pemReader.ReadObject();
            return keyParameter;
        }

        private static void UpdateDatabases(DbContext context, RoleManager<IdentityRole> roleManager)
        {
            // ReSharper disable once MethodHasAsyncOverload
            context.Database.Migrate();

            var roles = new[]
            {
                new IdentityRole("player"),
                new IdentityRole("publisher"),
            };

            foreach (var role in roles)
            {
                if (roleManager.RoleExistsAsync(role.Name).Result == false)
                {
                    roleManager.CreateAsync(role);
                }
            }
        }

        private static (Ed25519PublicKeyParameters publicKeyParameter, Ed25519PrivateKeyParameters privateKeyParameter) GenerateTempKeys()
        {
            var keyPairGenerator = new Ed25519KeyPairGenerator();
            keyPairGenerator.Init(new Ed25519KeyGenerationParameters(new SecureRandom()));
            var keyPair = keyPairGenerator.GenerateKeyPair();

            var privateKeyParameter = (Ed25519PrivateKeyParameters)keyPair.Private;
            var publicKeyParameter = (Ed25519PublicKeyParameters)keyPair.Public;

            return (publicKeyParameter, privateKeyParameter);
        }

        private static string GetPublicKeyString(Ed25519PublicKeyParameters publicKeyParameter)
        {
            var textWriter = new StringWriter();
            var pemWriter = new PemWriter(textWriter);

            pemWriter.WriteObject(publicKeyParameter);
            pemWriter.Writer.Flush();
            var publicKeyString = textWriter.ToString();

            return publicKeyString;
        }

        private (AsymmetricSecurityKey, AsymmetricSecurityKey, string) ReadKeys()
        {
            Ed25519PublicKeyParameters publicKeyParameter;
            Ed25519PrivateKeyParameters privateKeyParameter;

            if (CurrentEnvironment.IsProduction())
            {
                publicKeyParameter = ReadAsymmetricKeyParameterFromBase64String(Configuration["GxUserPublicKey"]) as Ed25519PublicKeyParameters;
                privateKeyParameter = ReadAsymmetricKeyParameterFromBase64String(Configuration["GxUserPrivateKey"]) as Ed25519PrivateKeyParameters;
            }
            else
            {
                (publicKeyParameter, privateKeyParameter) = GenerateTempKeys();
            }

            var publicKeyString = GetPublicKeyString(publicKeyParameter);

            var publicKey = new EdDsaSecurityKey(publicKeyParameter);
            var privateKey = new EdDsaSecurityKey(privateKeyParameter);

            return (publicKey, privateKey, publicKeyString);
        }
    }
}
