namespace Gx.User.Exceptions
{
    using System.Net;

    /// <summary>
    /// `401: Invalid username or password.` HTTP exception class.
    /// </summary>
    public class InvalidUserDataException : HttpException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidUserDataException"/> class.
        /// </summary>
        /// <param name="detail">Context of the error.</param>
        /// <param name="detailVisible">Flag indicates that detail should be returned in the response.</param>
        public InvalidUserDataException(object detail = null, bool detailVisible = false)
            : base(
                "Invalid username or password.",
                HttpStatusCode.Unauthorized,
                detail,
                detailVisible)
        {
        }
    }
}
