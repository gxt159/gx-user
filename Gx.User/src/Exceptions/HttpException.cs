namespace Gx.User.Exceptions
{
    using System;
    using System.Net;

    /// <summary>
    /// Base HTTP exception class.
    /// </summary>
    public class HttpException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HttpException"/> class.
        /// </summary>
        /// <param name="title">Short description of the error.</param>
        /// <param name="statusCode">Error related status code.</param>
        /// <param name="detail">Context of the error.</param>
        /// <param name="detailVisible">Flag indicates that detail should be returned in the response.</param>
        public HttpException(
            string title = "",
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError,
            object detail = null,
            bool detailVisible = false)
        {
            Title = title;
            StatusCode = statusCode;
            Detail = detail;
            DetailVisible = detailVisible;
        }

        /// <summary>
        /// Gets or sets short description of the error.
        /// </summary>
        /// <value>
        /// Short description of the error.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets error related status code.
        /// </summary>
        /// <value>
        /// Error related status code.
        /// </value>
        public HttpStatusCode? StatusCode { get; set; }

        /// <summary>
        /// Gets or sets context of the error.
        /// </summary>
        /// <value>
        /// Context of the error.
        /// </value>
        public object Detail { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether flag indicates that detail should be returned in the response.
        /// </summary>
        /// <value>
        /// A value indicating whether flag indicates that detail should be returned in the response.
        /// </value>
        public bool DetailVisible { get; set; }
    }
}
