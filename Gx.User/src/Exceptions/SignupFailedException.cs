namespace Gx.User.Exceptions
{
    using System.Net;

    /// <summary>
    /// `403: User with this username already registered.` HTTP exception class.
    /// </summary>
    public class SignupFailedException : HttpException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SignupFailedException"/> class.
        /// </summary>
        /// <param name="errors">Errors.</param>
        /// <param name="detail">Context of the error.</param>
        /// <param name="detailVisible">Flag indicates that detail should be returned in the response.</param>
        public SignupFailedException(string errors, object detail = null, bool detailVisible = false)
            : base(
                errors,
                HttpStatusCode.Forbidden,
                detail,
                detailVisible)
        {
        }
    }
}
