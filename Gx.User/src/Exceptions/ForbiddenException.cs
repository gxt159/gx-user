namespace Gx.User.Exceptions
{
    using System.Net;

    /// <summary>
    /// `403: Access denied.` HTTP exception class.
    /// </summary>
    public class ForbiddenException : HttpException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ForbiddenException"/> class.
        /// </summary>
        /// <param name="detail">Context of the error.</param>
        /// <param name="detailVisible">Flag indicates that detail should be returned in the response.</param>
        public ForbiddenException(object detail = null, bool detailVisible = false)
            : base(
                "Access denied.",
                HttpStatusCode.Forbidden,
                detail,
                detailVisible)
        {
        }
    }
}
