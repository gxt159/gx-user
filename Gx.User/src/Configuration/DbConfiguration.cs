namespace Gx.User.Configuration
{
    /// <summary>
    /// Configuration class for database.
    /// </summary>
    public class DbConfiguration
    {
        /// <summary>
        /// Gets server address.
        /// </summary>
        /// <value>
        /// Server address.
        /// </value>
        public string ServerAddress { get; init; }

        /// <summary>
        /// Gets username.
        /// </summary>
        /// <value>
        /// Username.
        /// </value>
        public string User { get; init; }

        /// <summary>
        /// Gets password.
        /// </summary>
        /// <value>
        /// Password.
        /// </value>
        public string Password { get; init; }

        /// <summary>
        /// Gets port.
        /// </summary>
        /// <value>
        /// Port.
        /// </value>
        public string Port { get; init; }

        /// <summary>
        /// Gets schema.
        /// </summary>
        /// <value>
        /// Schema.
        /// </value>
        public string Schema { get; init; }

        /// <summary>
        /// Gets database name.
        /// </summary>
        /// <value>
        /// Database name.
        /// </value>
        public string DatabaseName { get; init; }
    }
}
