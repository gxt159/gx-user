namespace Gx.User.Configuration
{
    using Microsoft.IdentityModel.Tokens;

    /// <summary>
    /// Configuration class of JWT in the project.
    /// </summary>
    public class JwtConfiguration
    {
        /// <summary>
        /// Gets or sets a public key in a JWT encoding alg.
        /// </summary>
        /// <value>
        /// A public key in a JWT encoding alg.
        /// </value>
        public AsymmetricSecurityKey PublicKey { get; set; }

        /// <summary>
        /// Gets or sets a public key in string form.
        /// </summary>
        /// <value>
        /// A public key in string form.
        /// </value>
        public string PublicKeyString { get; set; }

        /// <summary>
        /// Gets or sets a private key in a JWT encoding alg.
        /// </summary>
        /// <value>
        /// A private key in a JWT encoding alg.
        /// </value>
        public AsymmetricSecurityKey PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets an issuer of a JWT.
        /// </summary>
        /// <value>
        /// An issuer of a JWT.
        /// </value>
        public string Issuer { get; set; }

        /// <summary>
        /// Gets or sets an audience of a JWT.
        /// </summary>
        /// <value>
        /// An audience of a JWT.
        /// </value>
        public string Audience { get; set; }

        /// <summary>
        /// Gets or sets an token expiration time (in minutes) of a access JWT.
        /// </summary>
        /// <value>
        /// An token expiration time (in minutes) of a access JWT.
        /// </value>
        public int AccessTokenExpiration { get; set; }

        /// <summary>
        /// Gets or sets an token expiration time (in minutes) of a refresh JWT.
        /// </summary>
        /// <value>
        /// An token expiration time (in minutes) of a refresh JWT.
        /// </value>
        public int RefreshTokenExpiration { get; set; }
    }
}
