namespace Gx.User.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Provides an API for a health state of the application.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class HealthController : ControllerBase
    {
        /// <summary>
        /// Checks the application state.
        /// </summary>
        /// <returns>200 status code if everything is OK.</returns>
        /// <response code="200">Ping succeeded.</response>
        [HttpGet("ping")]
        public IActionResult Ping()
        {
            return Ok();
        }
    }
}
