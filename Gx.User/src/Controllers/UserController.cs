namespace Gx.User.Controllers
{
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Gx.User.Exceptions;
    using Gx.User.Models.Input;
    using Gx.User.Models.Output;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Provides an API for a user related actions.
    /// </summary>
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class UserController : ControllerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserController"/> class.
        /// </summary>
        /// <param name="userManager">Users' management API provider.</param>
        public UserController(UserManager<IdentityUser> userManager)
        {
            UserManager = userManager;
        }

        private UserManager<IdentityUser> UserManager { get; }

        /// <summary>
        /// Gets information about a user.
        /// </summary>
        /// <param name="dataToGetUser">Necessary input data to identify a user.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        [HttpGet("get/{id}")]
        public async Task<ActionResult<UserGetOutputModel>> Get([FromRoute] UserGetInputModel dataToGetUser)
        {
            var tokenUserId = User.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier);

            if (tokenUserId.Value != dataToGetUser.Id)
            {
                throw new ForbiddenException();
            }

            var user = await UserManager.FindByIdAsync(dataToGetUser.Id);
            var roles = await UserManager.GetRolesAsync(user);

            return new UserGetOutputModel
            {
                Id = user.Id,
                Roles = roles,
                Username = user.UserName,
            };
        }
    }
}
