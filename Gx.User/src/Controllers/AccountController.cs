namespace Gx.User.Controllers
{
    using System.Threading.Tasks;
    using Gx.User.Models.Input;
    using Gx.User.Models.Output;
    using Gx.User.Services;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Provides an API for the users' account related actions.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class AccountController : ControllerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="accountService">User account management API provider.</param>
        public AccountController(IAccountService accountService)
        {
            AccountService = accountService;
        }

        private IAccountService AccountService { get; }

        /// <summary>
        /// Sign in a user in the system.
        /// </summary>
        /// <param name="accountSigninInputModel">Necessary data to for account signin action.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        /// <response code="200">Signin succeeded.</response>
        /// <response code="401">Invalid username or password.</response>
        [HttpGet("signin")]
        [AllowAnonymous]
        public async Task<ActionResult<AccountSigninOutputModel>> Signin([FromQuery] AccountSigninInputModel accountSigninInputModel)
        {
            return await AccountService.SigninAsync(accountSigninInputModel);
        }

        /// <summary>
        /// Sign up a user in the system.
        /// </summary>
        /// <param name="accountSignupInputModel">Necessary data to for account signup action.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        /// <response code="200">Signup succeeded.</response>
        /// <response code="403">Sign up failed. Check errors.</response>
        [HttpPost("signup")]
        [AllowAnonymous]
        public async Task<ActionResult<AccountSignupOutputModel>> Signup([FromBody] AccountSignupInputModel accountSignupInputModel)
        {
            return await AccountService.SignupAsync(accountSignupInputModel);
        }

        /// <summary>
        /// Get new access and refresh tokens.
        /// </summary>
        /// <param name="refreshToken">Valid refresh token.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        /// <response code="200">Signup succeeded.</response>
        /// <response code="403">Refresh token is invalid.</response>
        [HttpGet("refresh")]
        [AllowAnonymous]
        public async Task<ActionResult<AccountRefreshOutputModel>> Refresh([FromQuery] string refreshToken)
        {
            return await AccountService.RefreshAsync(refreshToken);
        }

        /// <summary>
        /// Get GX public key used for checking signature.
        /// </summary>
        /// <response code="200">Returns public key.</response>
        /// <response code="500">Server error.</response>
        /// <returns>Public key.</returns>
        [HttpGet("publickey")]
        [AllowAnonymous]
        public ActionResult<string> GetPublicKey()
        {
            return AccountService.GetPublicKey();
        }
    }
}
