namespace Gx.User.Services
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using Gx.User.Configuration;
    using Gx.User.Models.DTO;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Options;
    using Microsoft.IdentityModel.Tokens;
    using Newtonsoft.Json;
    using ScottBrady.IdentityModel.Crypto;

    /// <inheritdoc />
    public class JwtService : IJwtService
    {
        private const string SecurityAlgorithm = ExtendedSecurityAlgorithms.EdDsa;

        /// <summary>
        /// Initializes a new instance of the <see cref="JwtService"/> class.
        /// </summary>
        /// <param name="configuration">JWT configuration extracted from the application configuration.</param>
        /// <param name="tokenValidationParameters">Current token validation parameters used in app.</param>
        public JwtService(IOptions<JwtConfiguration> configuration, TokenValidationParameters tokenValidationParameters)
        {
            TokenValidationParameters = tokenValidationParameters;
            Configuration = configuration.Value;
        }

        private JwtConfiguration Configuration { get; }

        private TokenValidationParameters TokenValidationParameters { get; }

        /// <inheritdoc/>
        public JwtAuthPair CreateTokens(IdentityUser user, IEnumerable<string> roles)
        {
            var requestTime = DateTime.Now;

            var accessJwt = new JwtSecurityToken(
                Configuration.Issuer,
                Configuration.Audience,
                GetAccessTokenClaims(user, roles),
                expires: requestTime.AddMinutes(Configuration.AccessTokenExpiration),
                signingCredentials: new SigningCredentials(
                    Configuration.PrivateKey,
                    SecurityAlgorithm));

            var refreshJwt = new JwtSecurityToken(
                Configuration.Issuer,
                Configuration.Audience,
                GetRefreshTokenClaims(user),
                expires: requestTime.AddMinutes(Configuration.RefreshTokenExpiration),
                signingCredentials: new SigningCredentials(
                    Configuration.PrivateKey,
                    SecurityAlgorithm));

            var jwtHandler = new JwtSecurityTokenHandler();
            var accessToken = jwtHandler.WriteToken(accessJwt);
            var refreshToken = jwtHandler.WriteToken(refreshJwt);

            return new JwtAuthPair
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken,
            };
        }

        /// <inheritdoc/>
        public ClaimsPrincipal ValidateToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();

                // Throws exception if something is wrong
                var principal = tokenHandler.ValidateToken(token, TokenValidationParameters, out var validatedToken);

                // ReSharper disable once ConvertIfStatementToReturnStatement
                if (IsJwtValidSecurityAlgorithm(validatedToken) == false)
                {
                    return null;
                }

                return principal;
            }
            catch
            {
                return null;
            }
        }

        /// <inheritdoc/>
        public string GetPublicKey()
        {
            return Configuration.PublicKeyString;
        }

        private static string GenerateRefreshTokenString()
        {
            var randomNumber = new byte[32];
            using var randomNumberGenerator = RandomNumberGenerator.Create();
            randomNumberGenerator.GetBytes(randomNumber);
            return Convert.ToBase64String(randomNumber);
        }

        private static IEnumerable<Claim> GetAccessTokenClaims(IdentityUser user, IEnumerable<string> roles)
        {
            return new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Role, JsonConvert.SerializeObject(roles)),
            };
        }

        private static IEnumerable<Claim> GetRefreshTokenClaims(IdentityUser user)
        {
            return new[]
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Sid, GenerateRefreshTokenString()),
            };
        }

        private static bool IsJwtValidSecurityAlgorithm(SecurityToken token)
        {
            return token is JwtSecurityToken jwtSecurityToken &&
                   jwtSecurityToken.Header.Alg.Equals(
                       SecurityAlgorithm,
                       StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
