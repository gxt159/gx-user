namespace Gx.User.Services
{
    using System.Linq;
    using System.Net;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Gx.User.Entities;
    using Gx.User.Exceptions;
    using Gx.User.Models.Input;
    using Gx.User.Models.Output;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;

    /// <inheritdoc />
    public class AccountService : IAccountService
    {
        private const string RefreshTokenName = "RefreshToken";
        private const string LoginProvider = "GX";

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountService"/> class.
        /// </summary>
        /// <param name="signInManager">Users' sign in API provider.</param>
        /// <param name="userManager">Users' management API provider.</param>
        /// <param name="jwtAuthManager">JWT related actions API provider.</param>
        /// <param name="userDbContext">User DB context.</param>
        public AccountService(
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            IJwtService jwtAuthManager,
            UserDbContext userDbContext)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            JwtAuthManager = jwtAuthManager;
            UserDbContext = userDbContext;
        }

        private SignInManager<IdentityUser> SignInManager { get; }

        private UserManager<IdentityUser> UserManager { get; }

        private IJwtService JwtAuthManager { get; }

        private UserDbContext UserDbContext { get; }

        /// <inheritdoc/>
        public async Task<AccountSigninOutputModel> SigninAsync(AccountSigninInputModel dataToSignin)
        {
            var user = await UserManager.FindByNameAsync(dataToSignin.Username);

            if (user == null)
            {
                throw new InvalidUserDataException();
            }

            var signInResult =
                await SignInManager.CheckPasswordSignInAsync(user, dataToSignin.Password, false);

            if (!signInResult.Succeeded)
            {
                throw new InvalidUserDataException();
            }

            var roles = await UserManager.GetRolesAsync(user);
            var jwtAuthResult = JwtAuthManager.CreateTokens(user, roles);

            // save refresh token in db
            await UserManager.SetAuthenticationTokenAsync(user, LoginProvider, RefreshTokenName, jwtAuthResult.RefreshToken);

            return new AccountSigninOutputModel
            {
                Id = user.Id,
                AccessToken = jwtAuthResult.AccessToken,
                RefreshToken = jwtAuthResult.RefreshToken,
            };
        }

        /// <inheritdoc/>
        public async Task<AccountSignupOutputModel> SignupAsync(AccountSignupInputModel dataToSignup)
        {
            var user = new IdentityUser
            {
                UserName = dataToSignup.Username,
            };

            var registerResult = await UserManager.CreateAsync(user, dataToSignup.Password);

            if (!registerResult.Succeeded)
            {
                throw new SignupFailedException(string.Join(" ", registerResult.Errors.Select(x => x.Description)));
            }

            var roles = dataToSignup.IsPublisher ? new[] { "publisher" } : new[] { "player" };
            var addToRolesResult = await UserManager.AddToRolesAsync(user, roles);

            if (!addToRolesResult.Succeeded)
            {
                // TODO change to normal exception.
                throw new HttpException();
            }

            var jwtAuthResult = JwtAuthManager.CreateTokens(user, roles);

            // save refresh token in db
            await UserManager.SetAuthenticationTokenAsync(user, LoginProvider, RefreshTokenName, jwtAuthResult.RefreshToken);

            var sql = dataToSignup.IsPublisher
                ? $"CALL add_publisher(uuid('{user.Id}'), '{user.UserName}')"
                : $"CALL add_player(uuid('{user.Id}'), '{user.UserName}')";
            await UserDbContext.Database.ExecuteSqlRawAsync(sql);

            return new AccountSignupOutputModel
            {
                Id = user.Id,
                AccessToken = jwtAuthResult.AccessToken,
                RefreshToken = jwtAuthResult.RefreshToken,
            };
        }

        /// <inheritdoc/>
        public async Task<AccountRefreshOutputModel> RefreshAsync(string refreshToken)
        {
            var principal = JwtAuthManager.ValidateToken(refreshToken);

            if (principal == null)
            {
                throw new ForbiddenException();
            }

            var username = principal.Claims.Single(x => x.Type == ClaimTypes.Name).Value;
            var user = await UserManager.FindByNameAsync(username);

            var savedRefreshToken =
                await UserManager.GetAuthenticationTokenAsync(user, LoginProvider, RefreshTokenName);

            if (refreshToken.Equals(savedRefreshToken) == false)
            {
                throw new ForbiddenException();
            }

            var roles = await UserManager.GetRolesAsync(user);
            var jwtAuthResult = JwtAuthManager.CreateTokens(user, roles);

            var setTokenResult = await UserManager.SetAuthenticationTokenAsync(user, LoginProvider, RefreshTokenName, jwtAuthResult.RefreshToken);

            if (setTokenResult != IdentityResult.Success)
            {
                throw new HttpException(statusCode: HttpStatusCode.InternalServerError, detail: setTokenResult.Errors);
            }

            return new AccountRefreshOutputModel
            {
                Id = user.Id,
                AccessToken = jwtAuthResult.AccessToken,
                RefreshToken = jwtAuthResult.RefreshToken,
            };
        }

        /// <inheritdoc/>
        public string GetPublicKey()
        {
            return JwtAuthManager.GetPublicKey();
        }
    }
}
