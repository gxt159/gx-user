namespace Gx.User.Services
{
    using System.Threading.Tasks;
    using Gx.User.Models.Input;
    using Gx.User.Models.Output;

    /// <summary>
    /// Service responsible for users' account actions.
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Signin a user in the system (creates a new user session).
        /// </summary>
        /// <param name="dataToSignin">Data, necessary to create a new user session.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        Task<AccountSigninOutputModel> SigninAsync(AccountSigninInputModel dataToSignin);

        /// <summary>
        /// Signup new user in the system (creates new session for this user, too).
        /// </summary>
        /// <param name="dataToSignup">Data, necessary to create a new user.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        Task<AccountSignupOutputModel> SignupAsync(AccountSignupInputModel dataToSignup);

        /// <summary>
        /// Validate refresh token and return new JWT tokens.
        /// </summary>
        /// <param name="refreshToken">Refresh token.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        Task<AccountRefreshOutputModel> RefreshAsync(string refreshToken);

        /// <summary>
        /// Get public key used for signing tokens.
        /// </summary>
        /// <returns>Public key.</returns>
        string GetPublicKey();
    }
}
