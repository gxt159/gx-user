namespace Gx.User.Services
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using Gx.User.Models.DTO;
    using Microsoft.AspNetCore.Identity;

    /// <summary>
    /// Service responsible for JWT related actions.
    /// </summary>
    public interface IJwtService
    {
        /// <summary>
        /// Creates a new pair of the user's JWT.
        /// </summary>
        /// <param name="user">Target user entity.</param>
        /// <param name="roles">User's roles.</param>
        /// <returns>A <see cref="JwtAuthPair"/> containing created tokens..</returns>
        JwtAuthPair CreateTokens(IdentityUser user, IEnumerable<string> roles);

        /// <summary>
        /// Validates token.
        /// </summary>
        /// <param name="token">Token to validate.</param>
        /// <returns>A <see cref="ClaimsPrincipal"/> with data in token. Null if token is not valid.</returns>
        ClaimsPrincipal ValidateToken(string token);

        /// <summary>
        /// Get public key used for signing tokens.
        /// </summary>
        /// <returns>String with public key.</returns>
        string GetPublicKey();
    }
}
