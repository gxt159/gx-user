namespace Gx.User.Entities
{
    using Gx.User.Configuration;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Migrations;
    using Microsoft.Extensions.Options;

    /// <summary>
    /// Database context of the application.
    /// </summary>
    public class UserDbContext : IdentityDbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContext"/> class.
        /// </summary>
        /// <param name="configuration">Database connection settings extracted from the application configuration.</param>
        public UserDbContext(IOptions<DbConfiguration> configuration)
        {
            DbConfiguration = configuration.Value;
        }

        private DbConfiguration DbConfiguration { get; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(
                $"Server={DbConfiguration.ServerAddress};" +
                $"Port={DbConfiguration.Port};" +
                $"Database={DbConfiguration.DatabaseName};" +
                $"Username={DbConfiguration.User};" +
                $"Password={DbConfiguration.Password};",
                builder => builder.MigrationsHistoryTable(HistoryRepository.DefaultTableName, DbConfiguration.Schema));
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema(DbConfiguration.Schema);
            base.OnModelCreating(builder);
        }
    }
}
