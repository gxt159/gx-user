namespace Gx.User.Models.Input
{
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Represents necessary input data for account signup action.
    /// </summary>
    public class AccountSignupInputModel
    {
        /// <summary>
        /// Gets or sets username of a new user.
        /// </summary>
        /// <value>
        /// Username of a new user.
        /// </value>
        [Required]
        [NotNull]
        [RegularExpression(@"\w{4,50}")]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets password of a new user.
        /// </summary>
        /// <value>
        /// Password of a new user.
        /// </value>
        [Required]
        [NotNull]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether if a new user is a publisher.
        /// </summary>
        /// <value>
        /// A value indicating whether if a new user is a publisher.
        /// </value>
        [Required]
        [NotNull]
        public bool IsPublisher { get; set; }
    }
}
