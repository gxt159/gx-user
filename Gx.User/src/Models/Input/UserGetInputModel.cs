namespace Gx.User.Models.Input
{
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Represents necessary input data for get user action.
    /// </summary>
    public class UserGetInputModel
    {
        /// <summary>
        /// Gets or sets identifier of a user.
        /// </summary>
        /// <value>
        /// Identifier of a user.
        /// </value>
        [Required]
        [NotNull]
        public string Id { get; set; }
    }
}
