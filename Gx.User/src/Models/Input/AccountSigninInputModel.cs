namespace Gx.User.Models.Input
{
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Represents necessary input data for account signin action.
    /// </summary>
    public class AccountSigninInputModel
    {
        /// <summary>
        /// Gets or sets username of a user.
        /// </summary>
        /// <value>
        /// Username of a user.
        /// </value>
        [Required]
        [NotNull]
        [RegularExpression(@"\w{4,50}")]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets password of a user.
        /// </summary>
        /// <value>
        /// Password of a user.
        /// </value>
        [Required]
        [NotNull]
        public string Password { get; set; }
    }
}
