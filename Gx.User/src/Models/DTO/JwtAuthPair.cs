namespace Gx.User.Models.DTO
{
    using System.Text.Json.Serialization;

    /// <summary>
    /// Access and refresh JWT pair.
    /// </summary>
    public class JwtAuthPair
    {
        /// <summary>
        /// Gets or sets access JWT value.
        /// </summary>
        /// <value>
        /// Access JWT value.
        /// </value>
        [JsonPropertyName("accessToken")]
        public string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets refresh JWT value.
        /// </summary>
        /// <value>
        /// Refresh JWT value.
        /// </value>
        [JsonPropertyName("refreshToken")]
        public string RefreshToken { get; set; }
    }
}
