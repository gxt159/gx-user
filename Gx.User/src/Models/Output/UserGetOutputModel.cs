namespace Gx.User.Models.Output
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents output data for get user action.
    /// </summary>
    public class UserGetOutputModel
    {
        /// <summary>
        /// Gets or sets identifier of a user.
        /// </summary>
        /// <value>
        /// Identifier of a user.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets username of a user.
        /// </summary>
        /// <value>
        /// Username of a user.
        /// </value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets user's roles in the application.
        /// </summary>
        /// <value>
        /// User's roles.
        /// </value>
        public IEnumerable<string> Roles { get; set; }
    }
}
