namespace Gx.User.Models.Output
{
    /// <summary>
    /// Represents output data for account signin action.
    /// </summary>
    public class AccountSigninOutputModel
    {
        /// <summary>
        /// Gets or sets identifier of a user.
        /// </summary>
        /// <value>
        /// Identifier of a user.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets user's access JWT value.
        /// </summary>
        /// <value>
        /// User's access JWT value.
        /// </value>
        public string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets user's refresh JWT value.
        /// </summary>
        /// <value>
        /// User's refresh JWT value.
        /// </value>
        public string RefreshToken { get; set; }
    }
}
