using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Gx.User.Configuration;
using Gx.User.Entities;
using Gx.User.Exceptions;
using Gx.User.Models.DTO;
using Gx.User.Models.Input;
using Gx.User.Models.Output;
using Gx.User.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace Gx.User.Tests.Unit.User.Services
{
    public class AccountServiceTest
    {
        private Mock<SignInManager<IdentityUser>> SignInManagerMock { get; }

        private Mock<UserManager<IdentityUser>> UserManagerMock { get; }

        private Mock<IJwtService> JwtServiceMock { get; }

        private Mock<UserDbContext> UserDbContextMock { get; }

        private IOptions<DbConfiguration> DbConfiguration { get; }

        public AccountServiceTest()
        {
            var mockUserStore = new Mock<IUserStore<IdentityUser>>();
            var mockContextAccessor = new Mock<IHttpContextAccessor>();
            var mockUserPrincipalFactory = new Mock<IUserClaimsPrincipalFactory<IdentityUser>>();

            UserManagerMock = new Mock<UserManager<IdentityUser>>(mockUserStore.Object, null, null, null, null, null,
                null, null, null);
            SignInManagerMock = new Mock<SignInManager<IdentityUser>>(UserManagerMock.Object,
                mockContextAccessor.Object, mockUserPrincipalFactory.Object, null, null, null, null);
            JwtServiceMock = new Mock<IJwtService>();

            var dbConfig = new DbConfiguration
            {
                ServerAddress = "test",
                User = "test",
                Password = "test",
                Port = "123",
                Schema = "test",
                DatabaseName = "test"
            };

            DbConfiguration = Options.Create(dbConfig);
            UserDbContextMock = new Mock<UserDbContext>(DbConfiguration);
        }

        [Fact]
        public async void When_SigninSuccessful_Returns_UserSigninOutputModel_OnSignin()
        {
            // Arrange.
            var userModel = new AccountSigninInputModel
            {
                Username = "test",
                Password = "test"
            };

            var user = new IdentityUser(userModel.Username);

            var jwtAuthPair = new JwtAuthPair
            {
                AccessToken = "1234",
                RefreshToken = "5678"
            };

            // Arrange behavior.
            SignInManagerMock.Setup(x =>
                    x.CheckPasswordSignInAsync(user, userModel.Password, false))
                .ReturnsAsync(SignInResult.Success);

            UserManagerMock.Setup(x => x.FindByNameAsync(userModel.Username))
                .ReturnsAsync(user);
            UserManagerMock.Setup(x => x.GetRolesAsync(It.IsAny<IdentityUser>())).ReturnsAsync(new List<string>());

            JwtServiceMock.Setup(x => x.CreateTokens(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
                .Returns(jwtAuthPair);

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object,
                JwtServiceMock.Object, UserDbContextMock.Object);

            // Act.
            var result = await sut.SigninAsync(userModel);

            //Assert.
            Assert.NotNull(result);
        }

        [Fact]
        public async void When_SigninSuccessful_RefreshTokenIsSaved_OnSignin()
        {
            // Arrange.
            var userModel = new AccountSigninInputModel
            {
                Username = "test",
                Password = "test"
            };

            var user = new IdentityUser(userModel.Username);

            var jwtAuthPair = new JwtAuthPair
            {
                AccessToken = "1234",
                RefreshToken = "5678"
            };

            // Arrange behavior.
            SignInManagerMock.Setup(x =>
                    x.CheckPasswordSignInAsync(user, userModel.Password, false))
                .ReturnsAsync(SignInResult.Success);

            UserManagerMock.Setup(x => x.FindByNameAsync(userModel.Username))
                .ReturnsAsync(user);
            UserManagerMock.Setup(x => x.GetRolesAsync(It.IsAny<IdentityUser>())).ReturnsAsync(new List<string>());
            UserManagerMock
                .Setup(x => x.SetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(),
                    It.IsAny<string>(), jwtAuthPair.RefreshToken)).ReturnsAsync(IdentityResult.Success);

            JwtServiceMock.Setup(x => x.CreateTokens(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
                .Returns(jwtAuthPair);

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object,
                JwtServiceMock.Object, UserDbContextMock.Object);

            // Act.
            await sut.SigninAsync(userModel);

            //Assert.
            UserManagerMock.Verify(
                x => x.SetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(), It.IsAny<string>(),
                    jwtAuthPair.RefreshToken), Times.Once);
        }

        [Fact]
        public async void When_UserNotFound_Throws_InvalidUserDataException_OnSignin()
        {
            // Arrange.
            var userModel = new AccountSigninInputModel
            {
                Username = "test",
                Password = "test"
            };

            // Arrange behavior.
            UserManagerMock.Setup(x => x.FindByNameAsync(userModel.Username))
                .ReturnsAsync((IdentityUser)null);

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object,
                JwtServiceMock.Object, UserDbContextMock.Object);

            // Act & Assert.
            await Assert.ThrowsAsync<InvalidUserDataException>(() => sut.SigninAsync(userModel));
        }

        [Fact]
        public async void When_SigninFailed_Throws_InvalidUserDataException_OnSignin()
        {
            // Arrange.
            var userModel = new AccountSigninInputModel
            {
                Username = "test",
                Password = "test"
            };

            var user = new IdentityUser(userModel.Username);

            // Arrange behavior.
            UserManagerMock.Setup(x => x.FindByNameAsync(userModel.Username))
                .ReturnsAsync(user);

            SignInManagerMock.Setup(x =>
                    x.CheckPasswordSignInAsync(user, userModel.Password, false))
                .ReturnsAsync(SignInResult.Failed);

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object,
                JwtServiceMock.Object, UserDbContextMock.Object);

            // Act & Assert.
            await Assert.ThrowsAsync<InvalidUserDataException>(() => sut.SigninAsync(userModel));
        }

        // [Fact]
        // public async void When_SignupSuccessful_Returns_UserSignupOutputModel_OnSignup()
        // {
        //     // Arrange.
        //     var userModel = new AccountSignupInputModel
        //     {
        //         Username = "test",
        //         Password = "test"
        //     };
        //
        //     var jwtAuthPair = new JwtAuthPair
        //     {
        //         AccessToken = "1234",
        //         RefreshToken = "5678"
        //     };
        //
        //     // Arrange behavior.
        //     UserManagerMock.Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
        //         .ReturnsAsync(IdentityResult.Success);
        //     UserManagerMock.Setup(x => x.AddToRolesAsync(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
        //         .ReturnsAsync(IdentityResult.Success);
        //
        //     JwtServiceMock.Setup(x => x.CreateTokens(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
        //         .Returns(jwtAuthPair);
        //
        //     // Arrange SUT.
        //     var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object,
        //         JwtServiceMock.Object, UserDbContextMock.Object);
        //
        //     // Act.
        //     var result = await sut.SignupAsync(userModel);
        //
        //     // Assert.
        //     Assert.NotNull(result);
        // }
        //
        // [Fact]
        // public async void When_SignupSuccessful_RefreshTokenIsSaved_OnSignup()
        // {
        //     // Arrange.
        //     var userModel = new AccountSignupInputModel
        //     {
        //         Username = "test",
        //         Password = "test"
        //     };
        //
        //     var jwtAuthPair = new JwtAuthPair
        //     {
        //         AccessToken = "1234",
        //         RefreshToken = "5678"
        //     };
        //
        //     // Arrange behavior.
        //     UserManagerMock.Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
        //         .ReturnsAsync(IdentityResult.Success);
        //     UserManagerMock.Setup(x => x.AddToRolesAsync(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
        //         .ReturnsAsync(IdentityResult.Success);
        //     UserManagerMock
        //         .Setup(x => x.SetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(),
        //             It.IsAny<string>(), jwtAuthPair.RefreshToken)).ReturnsAsync(IdentityResult.Success);
        //
        //     JwtServiceMock.Setup(x => x.CreateTokens(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
        //         .Returns(jwtAuthPair);
        //
        //     // Arrange SUT.
        //     var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object,
        //         JwtServiceMock.Object, UserDbContextMock.Object);
        //
        //     // Act.
        //     await sut.SignupAsync(userModel);
        //
        //     // Assert.
        //     UserManagerMock.Verify(
        //         x => x.SetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(), It.IsAny<string>(),
        //             jwtAuthPair.RefreshToken), Times.Once);
        // }

        [Fact]
        public async void When_SignupFailed_Throws_Exception_OnSignup()
        {
            // Arrange.
            var userModel = new AccountSignupInputModel
            {
                Username = "test",
                Password = "test"
            };

            // Arrange behavior.
            UserManagerMock.Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Failed());

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object,
                JwtServiceMock.Object, UserDbContextMock.Object);

            // Act & Assert.
            await Assert.ThrowsAsync<SignupFailedException>(() => sut.SignupAsync(userModel));
        }

        [Fact]
        public async void When_AddToRolesFailed_Throws_HttpException_OnSignup()
        {
            // Arrange.
            var userModel = new AccountSignupInputModel
            {
                Username = "test",
                Password = "test"
            };

            var jwtAuthPair = new JwtAuthPair
            {
                AccessToken = "1234",
                RefreshToken = "5678"
            };

            // Arrange behavior.
            UserManagerMock.Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);
            UserManagerMock.Setup(x => x.AddToRolesAsync(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
                .ReturnsAsync(IdentityResult.Failed());

            JwtServiceMock.Setup(x => x.CreateTokens(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
                .Returns(jwtAuthPair);

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object,
                JwtServiceMock.Object, UserDbContextMock.Object);

            // Act & Assert.
            await Assert.ThrowsAsync<HttpException>(() => sut.SignupAsync(userModel));
        }

        [Fact]
        public void Return_PublicKey_OnGetPublicKey()
        {
            // Arrange.
            const string publicKey = "qwerty";

            // Arrange behavior.
            JwtServiceMock.Setup(x => x.GetPublicKey()).Returns(publicKey);

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object, JwtServiceMock.Object,
                UserDbContextMock.Object);

            // Act.
            var result = sut.GetPublicKey();

            // Assert.
            Assert.Equal(publicKey, result);
        }

        [Fact]
        public async void When_RefreshTokenIsValid_Return_JwtTokens_OnRefresh()
        {
            // Arrange.
            const string inputRefreshToken = "1234";
            const string savedRefreshToken = inputRefreshToken;
            const string outputRefreshToken = "5678";

            var output = new AccountRefreshOutputModel
            {
                Id = "1",
                AccessToken = "1234",
                RefreshToken = outputRefreshToken
            };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, "username")
            };
            var identity = new ClaimsIdentity(claims, "TestAuthType");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            // Arrange behavior.
            UserManagerMock.Setup(x => x.FindByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(new IdentityUser { Id = output.Id });
            UserManagerMock.Setup(x =>
                    x.GetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(savedRefreshToken);
            UserManagerMock.Setup(x => x.GetRolesAsync(It.IsAny<IdentityUser>())).ReturnsAsync(new List<string>());
            UserManagerMock
                .Setup(x => x.SetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);

            JwtServiceMock.Setup(x => x.ValidateToken(It.IsAny<string>())).Returns(claimsPrincipal);
            JwtServiceMock.Setup(x => x.CreateTokens(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
                .Returns(new JwtAuthPair { AccessToken = output.AccessToken, RefreshToken = output.RefreshToken });

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object, JwtServiceMock.Object,
                UserDbContextMock.Object);

            // Act.
            var result = await sut.RefreshAsync(inputRefreshToken);

            // Assert.
            Assert.Equal(output.RefreshToken, result.RefreshToken);
        }

        [Fact]
        public async void When_RefreshTokenIsValid_RefreshTokenIsSaved_OnRefresh()
        {
            // Arrange.
            const string inputRefreshToken = "1234";
            const string savedRefreshToken = inputRefreshToken;
            const string outputRefreshToken = "5678";

            var output = new AccountRefreshOutputModel
            {
                Id = "1",
                AccessToken = "1234",
                RefreshToken = outputRefreshToken
            };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, "username")
            };
            var identity = new ClaimsIdentity(claims, "TestAuthType");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            // Arrange behavior.
            UserManagerMock.Setup(x => x.FindByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(new IdentityUser { Id = output.Id });
            UserManagerMock.Setup(x =>
                    x.GetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(savedRefreshToken);
            UserManagerMock.Setup(x => x.GetRolesAsync(It.IsAny<IdentityUser>())).ReturnsAsync(new List<string>());
            UserManagerMock
                .Setup(x => x.SetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);

            JwtServiceMock.Setup(x => x.ValidateToken(It.IsAny<string>())).Returns(claimsPrincipal);
            JwtServiceMock.Setup(x => x.CreateTokens(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
                .Returns(new JwtAuthPair { AccessToken = output.AccessToken, RefreshToken = output.RefreshToken });

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object, JwtServiceMock.Object,
                UserDbContextMock.Object);

            // Act.
            await sut.RefreshAsync(inputRefreshToken);

            // Assert.
            UserManagerMock.Verify(
                x => x.SetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async void When_RefreshTokenIsNotValid_Throws_ForbiddenException_OnRefresh()
        {
            // Arrange.
            const string inputRefreshToken = "1234";
            const string savedRefreshToken = inputRefreshToken;
            const string outputRefreshToken = "5678";

            var output = new AccountRefreshOutputModel
            {
                Id = "1",
                AccessToken = "1234",
                RefreshToken = outputRefreshToken
            };

            // Arrange behavior.
            UserManagerMock.Setup(x => x.FindByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(new IdentityUser { Id = output.Id });
            UserManagerMock.Setup(x =>
                    x.GetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(savedRefreshToken);
            UserManagerMock.Setup(x => x.GetRolesAsync(It.IsAny<IdentityUser>())).ReturnsAsync(new List<string>());
            UserManagerMock
                .Setup(x => x.SetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);

            JwtServiceMock.Setup(x => x.ValidateToken(It.IsAny<string>())).Returns((ClaimsPrincipal)null);
            JwtServiceMock.Setup(x => x.CreateTokens(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
                .Returns(new JwtAuthPair { AccessToken = output.AccessToken, RefreshToken = output.RefreshToken });

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object, JwtServiceMock.Object,
                UserDbContextMock.Object);

            // Act & Assert.
            await Assert.ThrowsAsync<ForbiddenException>(() => sut.RefreshAsync(inputRefreshToken));
        }

        [Fact]
        public async void When_RefreshTokenIsNotInDatabase_Throws_ForbiddenException_OnRefresh()
        {
            // Arrange.
            const string inputRefreshToken = "1234";
            const string outputRefreshToken = "5678";

            var output = new AccountRefreshOutputModel
            {
                Id = "1",
                AccessToken = "1234",
                RefreshToken = outputRefreshToken
            };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, "username")
            };
            var identity = new ClaimsIdentity(claims, "TestAuthType");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            // Arrange behavior.
            UserManagerMock.Setup(x => x.FindByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(new IdentityUser { Id = output.Id });
            UserManagerMock.Setup(x =>
                    x.GetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(string.Empty);
            UserManagerMock.Setup(x => x.GetRolesAsync(It.IsAny<IdentityUser>())).ReturnsAsync(new List<string>());
            UserManagerMock
                .Setup(x => x.SetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);

            JwtServiceMock.Setup(x => x.ValidateToken(It.IsAny<string>())).Returns(claimsPrincipal);
            JwtServiceMock.Setup(x => x.CreateTokens(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
                .Returns(new JwtAuthPair { AccessToken = output.AccessToken, RefreshToken = output.RefreshToken });

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object, JwtServiceMock.Object,
                UserDbContextMock.Object);

            // Act & Assert.
            await Assert.ThrowsAsync<ForbiddenException>(() => sut.RefreshAsync(inputRefreshToken));
        }

        [Fact]
        public async void When_RefreshTokenFailedToSaveToDatabase_Throws_HttpException_OnRefresh()
        {
            // Arrange.
            const string inputRefreshToken = "1234";
            const string savedRefreshToken = inputRefreshToken;
            const string outputRefreshToken = "5678";

            var output = new AccountRefreshOutputModel
            {
                Id = "1",
                AccessToken = "1234",
                RefreshToken = outputRefreshToken
            };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, "username")
            };
            var identity = new ClaimsIdentity(claims, "TestAuthType");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            // Arrange behavior.
            UserManagerMock.Setup(x => x.FindByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(new IdentityUser { Id = output.Id });
            UserManagerMock.Setup(x =>
                    x.GetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(savedRefreshToken);
            UserManagerMock.Setup(x => x.GetRolesAsync(It.IsAny<IdentityUser>())).ReturnsAsync(new List<string>());
            UserManagerMock
                .Setup(x => x.SetAuthenticationTokenAsync(It.IsAny<IdentityUser>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed());

            JwtServiceMock.Setup(x => x.ValidateToken(It.IsAny<string>())).Returns(claimsPrincipal);
            JwtServiceMock.Setup(x => x.CreateTokens(It.IsAny<IdentityUser>(), It.IsAny<IEnumerable<string>>()))
                .Returns(new JwtAuthPair { AccessToken = output.AccessToken, RefreshToken = output.RefreshToken });

            // Arrange SUT.
            var sut = new AccountService(SignInManagerMock.Object, UserManagerMock.Object, JwtServiceMock.Object,
                UserDbContextMock.Object);

            // Act & Assert.
            await Assert.ThrowsAsync<HttpException>(() => sut.RefreshAsync(inputRefreshToken));
        }
    }
}
