namespace Gx.User.Tests.Unit.User.Services
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using Gx.User.Configuration;
    using Gx.User.Services;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Options;
    using Microsoft.IdentityModel.Tokens;
    using Org.BouncyCastle.Crypto.Generators;
    using Org.BouncyCastle.Crypto.Parameters;
    using Org.BouncyCastle.Security;
    using ScottBrady.IdentityModel.Tokens;
    using Xunit;

    public class JwtServiceTest
    {
        private const string PublicKey = "publicKey";
        private TokenValidationParameters TokenValidationParameters { get; }

        private IOptions<JwtConfiguration> Configuration { get; }

        public JwtServiceTest()
        {
            var keyPairGenerator = new Ed25519KeyPairGenerator();
            keyPairGenerator.Init(new Ed25519KeyGenerationParameters(new SecureRandom()));
            var keyPair = keyPairGenerator.GenerateKeyPair();

            var privateKeyParameters = (Ed25519PrivateKeyParameters)keyPair.Private;
            var publicKeyParameters = (Ed25519PublicKeyParameters)keyPair.Public;

            var publicKey = new EdDsaSecurityKey(publicKeyParameters);
            var privateKey = new EdDsaSecurityKey(privateKeyParameters);

            var jwtConfiguration = new JwtConfiguration
            {
                Audience = "audience",
                Issuer = "issuer",
                AccessTokenExpiration = 5,
                RefreshTokenExpiration = 10,
                PublicKey = publicKey,
                PrivateKey = privateKey,
                PublicKeyString = PublicKey
            };

            Configuration = Options.Create(jwtConfiguration);

            TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtConfiguration.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = publicKey,
                ValidAudience = jwtConfiguration.Audience,
                ValidateAudience = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.FromMinutes(1)
            };
        }

        [Fact]
        public void Returns_JwtTokens_OnCreateTokens()
        {
            // Arrange.
            var user = new IdentityUser("test");
            var roles = new[] { "user" };

            // Arrange SUT.
            var sut = new JwtService(Configuration, TokenValidationParameters);

            // Act.
            var result = sut.CreateTokens(user, roles);

            // Assert.
            Assert.NotNull(result);
        }

        [Fact]
        public void When_TokenIsValid_ReturnClaimsPrincipal_OnValidate()
        {
            // Arrange.
            var user = new IdentityUser("test");
            var roles = new[] { "user" };

            // Arrange SUT.
            var sut = new JwtService(Configuration, TokenValidationParameters);

            // Act.
            var token = sut.CreateTokens(user, roles).AccessToken;
            var result = sut.ValidateToken(token);

            // Assert.
            Assert.NotNull(result);
        }

        [Fact]
        public void When_TokenIsNotValid_ReturnNull_OnValidate()
        {
            // Arrange.
            const string token = "invalidToken";

            // Arrange SUT.
            var sut = new JwtService(Configuration, TokenValidationParameters);

            // Act.
            var result = sut.ValidateToken(token);

            // Assert.
            Assert.Null(result);
        }

        [Fact]
        public void When_TokenContainsNotValidSecurityAlgorithm_ReturnNull_OnValidate()
        {
            // Arrange.
            var rsa = RSA.Create();
            var rsaKey = new RsaSecurityKey(rsa);

            var jwtSecurityToken = new JwtSecurityToken(
                Configuration.Value.Issuer,
                Configuration.Value.Audience,
                new[] { new Claim("test", "test") },
                expires: DateTime.Now.AddMinutes(Configuration.Value.AccessTokenExpiration),
                signingCredentials: new SigningCredentials(rsaKey, SecurityAlgorithms.RsaSha512)
            );

            var tokenValidationParametersWithRsaKey = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = Configuration.Value.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = rsaKey,
                ValidAudience = Configuration.Value.Audience,
                ValidateAudience = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.FromMinutes(1)
            };

            var jwtHandler = new JwtSecurityTokenHandler();
            var token = jwtHandler.WriteToken(jwtSecurityToken);

            // Arrange SUT.
            var sut = new JwtService(Configuration, tokenValidationParametersWithRsaKey);

            // Act.
            var result = sut.ValidateToken(token);

            // Assert.
            Assert.Null(result);
        }

        [Fact]
        public void Returns_PublicKey_OnGetPublicKey()
        {
            // Arrange.
            const string expected = PublicKey;

            // Arrange SUT.
            var sut = new JwtService(Configuration, TokenValidationParameters);

            // Act.
            var result = sut.GetPublicKey();

            // Assert.
            Assert.Equal(expected, result);
        }
    }
}
