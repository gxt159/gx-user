namespace Gx.User.Tests.Unit.User.Controllers
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using Gx.User.Controllers;
    using Gx.User.Exceptions;
    using Gx.User.Models.Input;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Xunit;

    public class UserControllerTest
    {
        private Mock<UserManager<IdentityUser>> UserManagerMock { get; }

        public UserControllerTest()
        {
            var mockUserStore = new Mock<IUserStore<IdentityUser>>();
            UserManagerMock = new Mock<UserManager<IdentityUser>>(mockUserStore.Object, null, null, null, null, null,
                null, null, null);
        }

        [Fact]
        public async void If_TokenIdEqualsUserId_Return_UserGetOutputModel_OnGet()
        {
            // Arrange.
            const string userId = "test";
            var input = new UserGetInputModel
            {
                Id = userId
            };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, userId)
            };
            var identity = new ClaimsIdentity(claims, "Bearer");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            var httpContext = new DefaultHttpContext
            {
                User = claimsPrincipal
            };

            // Arrange behavior.
            UserManagerMock.Setup(x => x.FindByIdAsync(userId)).ReturnsAsync(new IdentityUser());
            UserManagerMock.Setup(x => x.GetRolesAsync(It.IsAny<IdentityUser>())).ReturnsAsync(new List<string>());

            // Arrange SUT.
            var sut = new UserController(UserManagerMock.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext
                }
            };

            // Act.
            var result = await sut.Get(input);

            // Assert.
            Assert.NotNull(result.Value);
        }

        [Fact]
        public async void If_TokenIdNotEqualsUserId_Throw_ForbiddenException_OnGet()
        {
            // Arrange.
            const string userId = "test";
            var input = new UserGetInputModel
            {
                Id = userId
            };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, userId + "123")
            };
            var identity = new ClaimsIdentity(claims, "Bearer");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            var httpContext = new DefaultHttpContext
            {
                User = claimsPrincipal
            };

            // Arrange behavior.
            UserManagerMock.Setup(x => x.FindByIdAsync(userId)).ReturnsAsync(new IdentityUser());
            UserManagerMock.Setup(x => x.GetRolesAsync(It.IsAny<IdentityUser>())).ReturnsAsync(new List<string>());

            // Arrange SUT.
            var sut = new UserController(UserManagerMock.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext
                }
            };

            // Act & Assert.
            await Assert.ThrowsAsync<ForbiddenException>(() => sut.Get(input));
        }
    }
}
