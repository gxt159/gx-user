using Gx.User.Controllers;
using Gx.User.Models.Input;
using Gx.User.Models.Output;
using Gx.User.Services;
using Moq;
using Xunit;

namespace Gx.User.Tests.Unit.User.Controllers
{
    public class AccountControllerTest
    {
        [Fact]
        public async void When_SigninSucceeded_Returns_UserSignIn_OnSignin()
        {
            // Arrange.
            var input = new AccountSigninInputModel
            {
                Username = "test",
                Password = "test"
            };
            var output = new AccountSigninOutputModel
            {
                Id = "whatever"
            };

            var mockService = new Mock<IAccountService>();

            // Arrange behavior.
            mockService.Setup(x => x.SigninAsync(input)).ReturnsAsync(output);

            // Arrange SUT.
            var sut = new AccountController(mockService.Object);

            // Act.
            var actual = await sut.Signin(input);

            // Assert.
            Assert.Equal(output, actual.Value);
        }

        [Fact]
        public async void When_SignupFailed_Returns_UserSignIn_OnSignup()
        {
            // Arrange.
            var input = new AccountSignupInputModel
            {
                Username = "test",
                Password = "test"
            };
            var output = new AccountSignupOutputModel
            {
                Id = "whatever"
            };
            var mockService = new Mock<IAccountService>();

            // Arrange behavior.
            mockService.Setup(x => x.SignupAsync(input)).ReturnsAsync(output);

            // Arrange SUT.
            var sut = new AccountController(mockService.Object);

            // Act.
            var actual = await sut.Signup(input);

            // Assert.
            Assert.Equal(output, actual.Value);
        }

        [Fact]
        public async void ShouldCall_AccountServiceSignin_And_Return_Result_OnSignup()
        {
            // Arrange.
            var input = new AccountSignupInputModel
            {
                Username = "test",
                Password = "test"
            };

            var output = new AccountSignupOutputModel
            {
                Id = "1",
                AccessToken = "1234",
                RefreshToken = "5678"
            };

            var mockAccountService = new Mock<IAccountService>();

            // Arrange behavior.
            mockAccountService.Setup(x => x.SignupAsync(input)).ReturnsAsync(output);

            // Arrange SUT.
            var sut = new AccountController(mockAccountService.Object);

            // Act.
            var result = await sut.Signup(input);

            // Assert.
            mockAccountService.Verify(x => x.SignupAsync(input), Times.Once);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async void ShouldCall_AccountServiceSignup_And_Return_Result_OnSignup()
        {
            // Arrange.
            var input = new AccountSigninInputModel
            {
                Username = "test",
                Password = "test"
            };

            var output = new AccountSigninOutputModel
            {
                Id = "1",
                AccessToken = "1234",
                RefreshToken = "5678"
            };

            var mockAccountService = new Mock<IAccountService>();

            // Arrange behavior.
            mockAccountService.Setup(x => x.SigninAsync(input)).ReturnsAsync(output);

            // Arrange SUT.
            var sut = new AccountController(mockAccountService.Object);

            // Act.
            var result = await sut.Signin(input);

            // Assert.
            mockAccountService.Verify(x => x.SigninAsync(input), Times.Once);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async void ShouldCall_AccountServiceRefresh_And_Return_Result_OnRefresh()
        {
            // Arrange.
            const string inputRefreshToken = "qwerty";

            var output = new AccountRefreshOutputModel
            {
                Id = "1",
                AccessToken = "1234",
                RefreshToken = "5678"
            };

            var mockAccountService = new Mock<IAccountService>();

            // Arrange behavior.
            mockAccountService.Setup(x => x.RefreshAsync(inputRefreshToken)).ReturnsAsync(output);

            // Arrange SUT.
            var sut = new AccountController(mockAccountService.Object);

            // Act.
            var result = await sut.Refresh(inputRefreshToken);

            // Assert.
            mockAccountService.Verify(x => x.RefreshAsync(inputRefreshToken), Times.Once);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public void ShouldCall_AccountServiceGetPublicKey_And_Return_Result_OnGetPublicKey()
        {
            // Arrange.
            const string output = "qwerty";
            var mockAccountService = new Mock<IAccountService>();

            // Arrange behavior.
            mockAccountService.Setup(x => x.GetPublicKey()).Returns(output);

            // Arrange SUT.
            var sut = new AccountController(mockAccountService.Object);

            // Act.
            var result = sut.GetPublicKey();

            // Assert.
            mockAccountService.Verify(x => x.GetPublicKey(), Times.Once);
            Assert.Equal(output, result.Value);
        }
    }
}
