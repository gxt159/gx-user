// using Microsoft.AspNetCore.Builder;
//
// namespace Gx.User.Tests.Functional.Controllers
// {
//     using System;
//     using System.Net;
//     using System.Net.Http;
//     using System.Text;
//     using Gx.User.Exceptions;
//     using Gx.User.Models.Input;
//     using Gx.User.Services;
//     using Microsoft.AspNetCore.Hosting;
//     using Microsoft.AspNetCore.TestHost;
//     using Microsoft.Extensions.DependencyInjection;
//     using Microsoft.Extensions.Hosting;
//     using Moq;
//     using Newtonsoft.Json;
//     using Xunit;
//
//     // TODO: Сделать тестирование через моки (expectations).
//
//     /// <summary>
//     /// Test class of the user related actions API.
//     /// </summary>
//     public class AccountControllerTest
//     {
//         /// <summary>
//         /// Method signin should return.
//         /// </summary>
//         [Fact]
//         public async void When_SigninFailed_Returns_UnauthorizedResult_OnSignin()
//         {
//             // Arrange.
//             var input = new AccountSigninInputModel
//             {
//                 Username = "test",
//                 Password = "test",
//             };
//
//             var mockService = new Mock<IAccountService>();
//
//             // Arrange behavior.
//             mockService.Setup(x => x.SigninAsync(input)).ThrowsAsync(new UserAlreadyRegisteredException());
//
//             // Arrange SUT.
//             using var host = await new HostBuilder()
//                 .ConfigureWebHost(webBuilder =>
//                 {
//                     webBuilder
//                         .UseTestServer()
//                         .ConfigureServices(services =>
//                         {
//                             services.AddScoped<IAccountService, AccountService>();
//                             services.AddControllers();
//                         })
//                         .Configure(app =>
//                         {
//                             app.UseRouting();
//                             app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
//                         });
//                 })
//                 .StartAsync();
//             var client = host.GetTestClient();
//
//             var request = new HttpRequestMessage
//             {
//                 Method = HttpMethod.Get,
//                 RequestUri = new Uri("http://localhost/account/signin"),
//                 Content = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json"),
//             };
//
//             // Act.
//             var actual = await client.SendAsync(request);
//
//             // Assert.
//             Assert.Equal(HttpStatusCode.Unauthorized, actual.StatusCode);
//         }
//         //
//         // [Fact]
//         // public async void When_SigninSucceeded_Returns_UserSignIn_OnSignin()
//         // {
//         //     // Arrange.
//         //     var input = new AccountSigninInputModel
//         //     {
//         //         Username = "test",
//         //         Password = "test"
//         //     };
//         //     var output = new AccountSigninOutputModel
//         //     {
//         //         Id = "whatever"
//         //     };
//         //
//         //     var mockService = new Mock<IAccountService>();
//         //
//         //     // Arrange behavior.
//         //     mockService.Setup(x => x.SigninAsync(input)).ReturnsAsync(output);
//         //
//         //     // Arrange SUT.
//         //     var sut = new AccountController(mockService.Object);
//         //
//         //     // Act.
//         //     var actual = await sut.Signin(input);
//         //
//         //     // Assert.
//         //     Assert.Equal(output, actual.Value);
//         // }
//         //
//         // [Fact]
//         // public async void When_SignupFailed_Returns_UnauthorizedResult_OnSignup()
//         // {
//         //     // Arrange.
//         //     var input = new AccountSignupInputModel
//         //     {
//         //         Username = "test",
//         //         Password = "test"
//         //     };
//         //
//         //     var mockService = new Mock<IAccountService>();
//         //
//         //     // Arrange behavior.
//         //     mockService.Setup(x => x.SignupAsync(input)).ThrowsAsync(new Exception());
//         //
//         //     // Arrange SUT.
//         //     var sut = new AccountController(mockService.Object);
//         //
//         //     // Act.
//         //     var actual = await sut.Signup(input);
//         //
//         //     // Assert.
//         //     Assert.IsType<UnauthorizedResult>(actual.Result);
//         // }
//         //
//         // [Fact]
//         // public async void When_SignupFailed_Returns_UserSignIn_OnSignup()
//         // {
//         //     // Arrange.
//         //     var input = new AccountSignupInputModel
//         //     {
//         //         Username = "test",
//         //         Password = "test"
//         //     };
//         //     var output = new AccountSignupOutputModel
//         //     {
//         //         Id = "whatever"
//         //     };
//         //     var mockService = new Mock<IAccountService>();
//         //
//         //     // Arrange behavior.
//         //     mockService.Setup(x => x.SignupAsync(input)).ReturnsAsync(output);
//         //
//         //     // Arrange SUT.
//         //     var sut = new AccountController(mockService.Object);
//         //
//         //     // Act.
//         //     var actual = await sut.Signup(input);
//         //
//         //     // Assert.
//         //     Assert.Equal(output, actual.Value);
//         // }
//     }
// }
