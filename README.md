# Gx.User

User project for C# microservices

# Setup
1. Clone this repo

# Launch
This Gx.User repository comes with base configuration for launch in docker container.
### Pros
1. Requires only docker installed
2. Can be easily expanded and connected with other services, launched locally
### Cons
1. Slower
2. Needs more resources
3. Needs docker installation (you might face some issues with Windows installation)

Run this command in project root directory (look for .sln file location)
```
docker-compose -f ./Gx.User/docker-compose.yml up
```

In your CLI you should see something like:
```
...
gx-user_1  | watch : Polling file watcher is enabled
gx-user_1  | watch : Started
gx-user_1  | info: Microsoft.Hosting.Lifetime[0]
gx-user_1  |       Now listening on: http://[::]:8080
gx-user_1  | info: Microsoft.Hosting.Lifetime[0]
gx-user_1  |       Application started. Press Ctrl+C to shut down.
gx-user_1  | info: Microsoft.Hosting.Lifetime[0]
gx-user_1  |       Hosting environment: Production
gx-user_1  | info: Microsoft.Hosting.Lifetime[0]
gx-user_1  |       Content root path: /app
```

Now the service is available on http://0.0.0.0:8080


## Generating a keypair
To generate keypair for jwt signing:

```bash
openssl genpkey -algorithm ed25519 -outform PEM -out id_rsa
openssl pkey -outform PEM -pubout -in id_rsa > id_rsa.pub
```

### Note
App launch with `dotnet watch run` by default. This command provides an opportunity to hot rebuild your application when your source code changes.

If you want to use simple `dotnet run`, then you should edit `./images/dev.Dockerfile` as you wish.


