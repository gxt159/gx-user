CREATE OR REPLACE FUNCTION auth.create_player()
   RETURNS TRIGGER
   LANGUAGE PLPGSQL
AS $$
BEGIN
   INSERT INTO public.players("Id","Username","Money")
   VALUES(uuid(NEW."Id"),NEW."UserName",0);

   RETURN NEW;
END;
$$
;
DROP TRIGGER IF EXISTS create_player_after_sign_up ON auth."AspNetUsers";
CREATE TRIGGER create_player_after_sign_up
	AFTER INSERT
	ON auth."AspNetUsers"
	FOR EACH ROW
	EXECUTE PROCEDURE auth.create_player();

